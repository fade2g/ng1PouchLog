'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl',
    controllerAs: '$ctrl'
  });
}])

.controller('View2Ctrl', ['$log', 'replicationService', function($log, replicationService) {
  this.startReplication = function() {
    replicationService.startReplication();
  };
  this.stopReplication = function() {
    replicationService.stopReplication();
  };
}]);
