(function(angular, PouchDB) {
  'use strict';
  angular.module('myApp')
    .provider('PouchLogger', function PouchLoggerProvider() {
      this.$get = [function pouchLoggerFactory() {
        return new PouchDB('logger');
      }];
    });
})(angular, PouchDB);
