(function(angular, PouchDB) {
  'use strict';
  angular.module('myApp')
    .provider('PouchRemoteLogger', function PouchRemoteLoggerProvider() {
      var remoteUrl;
      var pouchRemoteConfig = {};

      this.setRemoteUrl = function(url) {
        remoteUrl = url;
      };

      this.useCredentials = function(useCredentials) {
        if (useCredentials === false) {
          pouchRemoteConfig.ajax = {
            withCredentials: false
          }
        }
      };

      this.$get = [function pouchRemoteLoggerFactory() {
        return new PouchDB(remoteUrl, pouchRemoteConfig);
      }];
    });
})(angular, PouchDB);
