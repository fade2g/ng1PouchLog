(function (angular) {
  'use strict';

// Declare app level module which depends on views, and components
  angular.module('myApp')
    .config(['$provide', 'PouchRemoteLoggerProvider', function ($provide) {
      var levels = ['error', 'warn', 'info'];   // loglevels that shall be decorated, must be method names of $log

      $provide.decorator('$log', ['$delegate', 'PouchLogger', 'uuidService', function ($delegate, pouchLogger, uuidService) {

        function docFactory(level, input) {
          var timestamp = new Date();
          return {
            _id: '' + uuidService.uuid(),
            timestamp: timestamp,
            loglevel: level,
            content: input
          }
        }

        function delegateFunctionFactory (level, $delegate) {
          var originalFunction = $delegate[level];
          $delegate[level] = function() {
            pouchLogger.put(docFactory(level, Array.from(arguments)));
            originalFunction.apply(undefined, arguments);
          }
        }

        // place decorator around the given log levels according to the levels array
        levels.forEach(function(level) {
          delegateFunctionFactory(level, $delegate);
        });

        return $delegate;
      }])
    }]);
})(angular);
