(function (angular) {
    'use strict';

    angular.module('myApp')
      .service('uuidService', ['$window', UuidService]);


    /**
     * @ngdoc service
     * @name uuidService
     * @param $window
     * @return {{uuid: uuid}}
     * @description
     * This services provided UUID related functionality
     *
     */
    function UuidService($window) {
      // static lookup table for uuid
      var lut = [];
      for (var i = 0; i < 256; i++) {
        lut[i] = (i < 16 ? '0' : '') + (i).toString(16);
      }


      var randomFunction = (function createRandumFunction() {
        function fallback() {
          return [
            Math.random() * 0x100000000 >>> 0,
            Math.random() * 0x100000000 >>> 0,
            Math.random() * 0x100000000 >>> 0,
            Math.random() * 0x100000000 >>> 0]
        }

        function crypto() {
          try {
            var rnd = new Uint32Array(4);
            cryptoObj.getRandomValues(rnd);
            return rnd;
          } catch (e) {
            // call fallback in case of an error
            return fallback();
          }
        }

        var cryptoObj = $window.crypto || $window.msCrypto;
        if (angular.isDefined(cryptoObj) && angular.isDefined(cryptoObj.getRandomValues)) {
          return crypto
        } else {
          return fallback
        }
      })();

      /**
       * ngdoc method
       * @name uuidService#uuid
       * @methodOf uuid
       * @return {string} UUID string conforming RFC4122 (hopefully)
       * @description
       * This method generates a UUID that conforms RFC4122. It's based on the implementation
       * from http://stackoverflow.com/a/21963136 and the comment from Dave
       * A RFC4122 UUID has the structure 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx',
       * for example f81d4fae-7dec-11d0-a765-00a0c91e6bf6
       */
      function uuid() {
        var numbers = randomFunction();

        return lut[numbers[0] & 0xff] + lut[numbers[0] >> 8 & 0xff] + lut[numbers[0] >> 16 & 0xff] + lut[numbers[0] >> 24 & 0xff] + '-' +
          lut[numbers[1] & 0xff] + lut[numbers[1] >> 8 & 0xff] + '-' + lut[numbers[1] >> 16 & 0x0f | 0x40] + lut[numbers[1] >> 24 & 0xff] + '-' +
          lut[numbers[2] & 0x3f | 0x80] + lut[numbers[2] >> 8 & 0xff] + '-' + lut[numbers[2] >> 16 & 0xff] + lut[numbers[2] >> 24 & 0xff] +
          lut[numbers[3] & 0xff] + lut[numbers[3] >> 8 & 0xff] + lut[numbers[3] >> 16 & 0xff] + lut[numbers[3] >> 24 & 0xff];
      }

      return {
        uuid: uuid
      }
    }

  })(angular);
