(function(angular) {
  'use strict';

  angular.module('myApp')
    .service('replicationService', ['$interval', 'PouchLogger', 'PouchRemoteLogger', ReplicationService]);

  function ReplicationService($interval, pouchLogger, pouchRemoteLogger) {
    var running;
    function startReplication() {
      running = $interval(function() {
        pouchLogger.replicate.to(pouchRemoteLogger).on('complete', function () {
          console.log('replication done');
        }).on('error', function (err) {
          console.log('Replication failed due to ', err);
        }).on('change', function (info) {
          console.log('change:', info);
        }).on('complete', function (result) {
          console.log('coplete:', result)
        });
      }, 5000);
    }
    function stopReplication() {
      $interval.cancel(running);
    }
    return {
      startReplication: startReplication,
      stopReplication: stopReplication
    }
  }

})(angular);
