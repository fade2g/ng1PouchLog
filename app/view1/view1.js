'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl',
    controllerAs: '$ctrl'
  });
}])

.controller('View1Ctrl', ['$log', function($log) {
  $log.info('Switched to view 1');

  this.message = '';
  this.selectedLevel = undefined;

  this.loglevels = [
    {id: 0, text: 'error'},
    {id: 1, text: 'warn'},
    {id: 2, text: 'info'},
    {id: 3, text: 'debug'}
  ];

  this.submit = function() {
    $log[this.selectedLevel.text](this.message);
  }
}]);
