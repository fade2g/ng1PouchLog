'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'ngMaterial',
    'myApp.view1',
    'myApp.view2',
    'myApp.version'
  ])
  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider.otherwise({redirectTo: '/view1'});
  }])
  .config(['PouchRemoteLoggerProvider',  function (pouchRemoteLoggerProvider) {
    pouchRemoteLoggerProvider.useCredentials(false);
    pouchRemoteLoggerProvider.setRemoteUrl('http://localhost:3000/db/pouchLog');
  }
  ])
  .run(['replicationService', function (replicationService) {
    replicationService.startReplication('a', 'b');
  }]);
