# ng1PouchLog - Using pouchdb client side and server side

This project is an exercise in usage of pouchdb on the client an replicating the data to a remote pouch db.

It establishes a $log interceptor that will store log messages in a client side pouch db. This data
will then be replicated to the server side (another server). The ultimate goal ist, that the replication to
the server is being done periodically and that the client db is cleaned from old entries that are successfully
replicated.

It's based on angular seed project and uses a bit of angular material. But aesthetics don't matter here; it's not
about design but about technology.

# installing to play around
If you just want to start a new project without the commit history and remove the git repo completely then you can do:

```bash
git clone --depth=1 https://gitlab.com/fade2g/ng1PouchLog.git <your-project-name>
cd <your-project-name>
rm -rf .git
```

The you can install npm and bower dependencies:
```bash
npm install
```

The project consists of two independent parts:
* client app: The angular app that lets you register log entries
* node based express server: The backend that starts a pouchdb (in memory) server and offers a you to just check the in memory db

You will need two command prompts to start both parts.

Starting the App:
```bash
npm start
```

The you can go to the app on localhost:8000/app/

Starting the DB backend
```bash
npm run start-server
```

You can see a plain text dump of the data when you go to  localhost:8000/spool/
