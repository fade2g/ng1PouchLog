'use strict';

var express = require('express');
var app = express();
var PouchDB = require('pouchdb');
var cors = require('cors');         // cors middleware

var InMemPouchDB = PouchDB.defaults({db: require('memdown')});
var expressPouch = require('express-pouchdb');
var myPouch = new InMemPouchDB('pouchLog');

// cors handling
var corsOptions = {
  origin: "http://localhost:8000"
};

app.use('/db/', cors(corsOptions), expressPouch(InMemPouchDB));

app.use('/spool', function(req, res, next) {
  console.log('Will spool contents');
  myPouch.allDocs({include_docs: true}).then(function(data) {
    console.log('pouch returned data');
    res.send(data);
  });
});

app.listen(3000);
